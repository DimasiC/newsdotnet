﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace NewsDotNet.DomainModel.Entities
{
    ///<summary>
    ///is a tag of Article</summary>
    public class Tag
    {
        ///<summary>
        ///is an id spesificator</summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        ///<summary>
        ///is a name</summary>
        [StringLength(30)]
        [Required]
        public string Name { get; set; }
        public virtual ICollection<Article> Articles { get; set; }
    }
}
