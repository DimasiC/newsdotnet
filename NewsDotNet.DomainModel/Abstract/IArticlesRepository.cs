﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsDotNet.DomainModel.Entities;

namespace NewsDotNet.DomainModel.Abstract
{
    public interface IArticlesRepository
    {
        IEnumerable<Article> All();
        Article GetById(int id);
        Article GetByAddressName(string addressName);
        IEnumerable<Article> GetAllWithTag(int tagId);
        void Add(Article newArticle);
        void Update(Article articleToUpdate);
        void Delete(Article articleToDelete);

    }
}
