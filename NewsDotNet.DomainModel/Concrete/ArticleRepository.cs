﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsDotNet.DomainModel.Abstract;
using NewsDotNet.DomainModel.Entities;

namespace NewsDotNet.DomainModel.Concrete
{
    public class ArticleRepository:IArticlesRepository
    {
        private readonly EFDBContext _articleСontext =new EFDBContext();
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Article> All()
        {
            return _articleСontext.Articles.ToList();
        }
        public Article GetById(int id)
        {
            return _articleСontext.Articles.FirstOrDefault(t => t.Id == id);
        }
        public Article GetByAddressName(string addressName)
        {
            return _articleСontext.Articles.FirstOrDefault(a => a.AddressName == addressName);
        }
        public IEnumerable<Article> GetAllWithTag(int tagId)
        {

            return _articleСontext.Articles.Where(t => t.Id == tagId).ToList();
        }
        public void Add(Article newArticle)
        {
            _articleСontext.Articles.Add(new Entities.Article { Id = newArticle.Id });
            _articleСontext.SaveChanges();
        }
        public void Update(Article articleToUpdate)
        {
            _articleСontext.Articles.Attach(new Entities.Article { Id = articleToUpdate.Id });
            _articleСontext.SaveChanges();
        }
        public void Delete(Article articleToDelete)
        {
            _articleСontext.Articles.Remove(new Entities.Article{ Id = articleToDelete.Id });
            _articleСontext.SaveChanges();
        }
    }
}
