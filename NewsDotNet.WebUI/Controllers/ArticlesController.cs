﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewsDotNet.DomainModel.Entities;
using NewsDotNet.DomainModel.Abstract;

namespace NewsDotNet.WebUI.Controllers
{
    public class ArticlesController : Controller
    {
        private IArticlesRepository _articleRepo;

        public ArticlesController(IArticlesRepository articleRepo)
        {
            _articleRepo = articleRepo;
        }
        
        
        public ActionResult Show(string addressName)
        {
            var article = _articleRepo.GetByAddressName(addressName);
            if (null == article || article.State != ArticleStates.Published)
                return HttpNotFound();
            return View("Article", article);
        }

        public ActionResult Archive()
        {
            var articles = _articleRepo.All().Where(a => a.State == ArticleStates.Draft).OrderByDescending(a => a.CreatedTime).ToList();
            return View("List",articles);
        }
	}
}