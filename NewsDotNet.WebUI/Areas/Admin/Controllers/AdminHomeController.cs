﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewsDotNet.DomainModel.Entities;
using NewsDotNet.DomainModel.Abstract;

namespace NewsDotNet.WebUI.Areas.Admin.Controllers
{
    public class AdminHomeController : Controller
    {
        private IArticlesRepository _articleRepo;

        public AdminHomeController(IArticlesRepository articleRepo)
        {
            _articleRepo = articleRepo;
        }

        //
        // GET: /Admin/Home/
        public ActionResult Index()
        {
            return View(_articleRepo.All());
        }
	}
}