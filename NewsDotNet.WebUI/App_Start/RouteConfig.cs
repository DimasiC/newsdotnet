﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NewsDotNet.WebUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            
            routes.MapRoute(
                          name: "Archive",
                          url: "Articles/Archive",
                          defaults: new { controller = "Articles", action = "Archive" }
                          );
            routes.MapRoute(
                name: "ShowArticle",
                url: "Articles/{addressName}",
                defaults: new { controller = "Articles", action = "Show" }
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
